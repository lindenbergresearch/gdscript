package tscn

import com.intellij.openapi.util.IconLoader
import gdscript.GdIcon

object TscnIcon {

    val FILE = IconLoader.getIcon("icons/tscnFile.png", GdIcon::class.java)

}