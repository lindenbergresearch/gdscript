# Features

### Auto-completion  
![](../screens/features/autocompletion.png)
##### Inheritance & ClassName
![](../screens/features/autocompletion_classname.png)  
##### Annotations
![](../screens/features/autocompletion_annotation.png)
##### func overrides
![](../screens/features/autocompletion_functions.png)
##### Resources (`$Path/Node` && `$"%Name"`)
![](../screens/features/autocompletion_resources.png)
##### Inputs, Groups, Meta fields, user resources
![](../screens/features/string_completion.png)

### Refactoring
![](../screens/features/refactor.png)
### Go to declaration / usages
![](../screens/features/usages.png)
### File templates taken from Godot's source
![](../screens/features/file_template.png)
### Hides _prefix as private fields (optional based on Language settings)
![](../screens/features/_hide.png)
### Built-in documentation (Ctrl+Q)
![](../screens/features/documentation.png)

### Line markers
[//]: # (    - Resource usages)
[//]: # (    - Signals)
##### Super method
![](../screens/features/super_method.png)
##### Color picker
![](../screens/features/color_picker.png)
##### Run current scene
![](../screens/features/run_marker.png)


### Inlay hints
![](../screens/features/inlay.png)
### Param hints (Ctrl+P)
![](../screens/features/param_hint.png)
### Run configuration - start game from Editor
![](../screens/features/run_configuration.png)
### Formatter
![](../screens/features/formatter.png)
### is/has conditioned type for validations (ignore checks following get_node)
![](../screens/features/is_has.png)

# Actions
### Quick fixes
##### Add/change return Type
![](../screens/features/specify_variable.png)
##### Generate get_set methods
![](../screens/features/create_set_method.png)
##### Remove annotation
![](../screens/features/remove_annotation.png)
##### Change class_name to match filename
![](../screens/features/match_classname.png)
##### Remove getter & setter
![](../screens/features/remove_get_set.png)
##### Too many arguments / change function type
![](../screens/features/too_many_arguments.png)  
![](../screens/features/change_param.png)