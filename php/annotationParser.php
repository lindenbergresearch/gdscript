<?php

$filename = "@GDScript.xml";
$target   = "../src/main/kotlin/gdscript/utils/GdAnnotationUtil.kt";

$baseContent = "package gdscript.utils

import gdscript.model.GdAnnotation
import gdscript.psi.GdAnnotationTl

/**
 * Do not edit manually
 * Generated by annotationParser.php
 */
object GdAnnotationUtil {

    fun get(annotation: GdAnnotationTl): GdAnnotation? {
        return get(annotation.annotationType.text)
    }

    fun get(name: String): GdAnnotation? {
        return ANNOTATIONS[name.trimStart('@')]
    }

    // Name -> GdAnnotation
    val ANNOTATIONS = mapOf(
%s
    )

}
";

$data    = "";
$content = file_get_contents(sprintf("./godot-master/modules/gdscript/doc_classes/%s", $filename));
$xml     = (array)simplexml_load_string($content);

foreach ($xml['annotations'] ?? [] as $value) {
    $required = 0;
    $value    = (array)$value;
    $att      = (array)$value['@attributes'];

    $name       = substr($att['name'], 1);
    $qualifiers = $att['qualifiers'] ?? '' === 'vararg';
    $vararg     = $qualifiers ? 'true' : 'false';
    $line       = "";

    $any = false;
    foreach ($value['param'] ?? [] as $index => $param) {
        $any = true;
        if ($index === 0) $line .= "\n";

        $param = (array)$param;
        $p_att = $param['@attributes'] ?? [];
        $pName = $p_att['name'] ?? '';
        $type  = $p_att['type'] ?? 'Variant';
        $line  .= "            \"$pName\" to \"$type\",\n";

        if (!array_key_exists('default', $p_att)) {
            $required++;
        }
    }

    $data .= "        \"$name\" to GdAnnotation($vararg, $required, linkedMapOf(";
    $data .= $line;
    if ($any) {
        $data .= "        ";
    }
    $data .= ")),\n";
}

file_put_contents($target, sprintf($baseContent, $data));
