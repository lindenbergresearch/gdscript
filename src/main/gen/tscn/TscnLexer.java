/* The following code was generated by JFlex 1.7.0 tweaked for IntelliJ platform */

package tscn;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import tscn.psi.TscnTokenType;
import tscn.psi.TscnTypes;
import java.util.List;
import java.util.HashMap;
import java.util.Stack;


/**
 * This class is a scanner generated by 
 * <a href="http://www.jflex.de/">JFlex</a> 1.7.0
 * from the specification file <tt>Tscn.flex</tt>
 */
class TscnLexer implements FlexLexer {

  /** This character denotes the end of file */
  public static final int YYEOF = -1;

  /** initial size of the lookahead buffer */
  private static final int ZZ_BUFFERSIZE = 16384;

  /** lexical states */
  public static final int YYINITIAL = 0;
  public static final int HEADER = 2;
  public static final int VALUE = 4;
  public static final int DATA_LINE = 6;
  public static final int DATA_VALUE = 8;

  /**
   * ZZ_LEXSTATE[l] is the state in the DFA for the lexical state l
   * ZZ_LEXSTATE[l+1] is the state in the DFA for the lexical state l
   *                  at the beginning of a line
   * l is of the form l = 2*k, k a non negative integer
   */
  private static final int ZZ_LEXSTATE[] = { 
     0,  0,  1,  1,  2,  2,  3,  3,  4, 4
  };

  /** 
   * Translates characters to character classes
   * Chosen bits are [7, 7, 7]
   * Total runtime size is 1928 bytes
   */
  public static int ZZ_CMAP(int ch) {
    return ZZ_CMAP_A[(ZZ_CMAP_Y[ZZ_CMAP_Z[ch>>14]|((ch>>7)&0x7f)]<<7)|(ch&0x7f)];
  }

  /* The ZZ_CMAP_Z table has 68 entries */
  static final char ZZ_CMAP_Z[] = zzUnpackCMap(
    "\1\0\103\200");

  /* The ZZ_CMAP_Y table has 256 entries */
  static final char ZZ_CMAP_Y[] = zzUnpackCMap(
    "\1\0\1\1\53\2\1\3\22\2\1\4\37\2\1\3\237\2");

  /* The ZZ_CMAP_A table has 640 entries */
  static final char ZZ_CMAP_A[] = zzUnpackCMap(
    "\11\0\1\5\1\7\2\4\1\2\22\0\1\5\14\0\1\1\1\0\1\3\12\1\1\0\1\6\1\0\1\26\3\0"+
    "\32\1\1\27\1\0\1\25\1\0\1\12\1\0\2\1\1\14\1\11\1\15\1\1\1\10\1\1\1\24\4\1"+
    "\1\16\1\22\2\1\1\21\1\13\1\20\1\23\2\1\1\17\2\1\1\0\1\1\10\0\1\4\32\0\1\4"+
    "\337\0\1\4\177\0\13\4\35\0\2\4\5\0\1\4\57\0\1\4\40\0");

  /** 
   * Translates DFA states to action switch labels.
   */
  private static final int [] ZZ_ACTION = zzUnpackAction();

  private static final String ZZ_ACTION_PACKED_0 =
    "\5\0\1\1\1\2\1\1\1\3\1\4\1\2\4\4"+
    "\1\5\1\6\1\7\1\4\1\10\1\11\2\12\2\13"+
    "\13\4\1\14\11\4\1\15\4\4\1\16\2\4\1\17";

  private static int [] zzUnpackAction() {
    int [] result = new int[55];
    int offset = 0;
    offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAction(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /** 
   * Translates a state to a row index in the transition table
   */
  private static final int [] ZZ_ROWMAP = zzUnpackRowMap();

  private static final String ZZ_ROWMAP_PACKED_0 =
    "\0\0\0\30\0\60\0\110\0\140\0\170\0\220\0\250"+
    "\0\170\0\300\0\330\0\360\0\u0108\0\u0120\0\u0138\0\220"+
    "\0\220\0\60\0\u0150\0\220\0\u0168\0\220\0\u0180\0\u0198"+
    "\0\220\0\u01b0\0\u01c8\0\u01e0\0\u01f8\0\u0210\0\u0228\0\u0240"+
    "\0\u0258\0\u0270\0\u0288\0\u02a0\0\300\0\u02b8\0\u02d0\0\u02e8"+
    "\0\u0300\0\u0318\0\u0330\0\u0348\0\u0360\0\u0378\0\300\0\u0390"+
    "\0\u03a8\0\u03c0\0\u03d8\0\300\0\u03f0\0\u0408\0\300";

  private static int [] zzUnpackRowMap() {
    int [] result = new int[55];
    int offset = 0;
    offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackRowMap(String packed, int offset, int [] result) {
    int i = 0;  /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int high = packed.charAt(i++) << 16;
      result[j++] = high | packed.charAt(i++);
    }
    return j;
  }

  /** 
   * The transition table of the DFA
   */
  private static final int [] ZZ_TRANS = zzUnpackTrans();

  private static final String ZZ_TRANS_PACKED_0 =
    "\2\6\1\7\3\6\1\10\1\7\17\6\1\11\1\0"+
    "\1\12\3\0\1\13\2\0\1\14\3\12\1\15\1\16"+
    "\1\17\6\12\1\20\1\21\1\0\2\22\1\0\4\22"+
    "\1\0\20\22\2\23\1\0\1\23\1\0\1\13\1\23"+
    "\1\0\16\23\1\24\1\23\2\25\1\26\2\25\1\27"+
    "\1\25\1\26\20\25\2\6\1\0\4\6\1\0\20\6"+
    "\30\0\2\10\1\30\4\10\1\31\20\10\1\0\1\12"+
    "\1\0\1\12\4\0\15\12\10\0\1\13\23\0\1\12"+
    "\1\0\1\12\4\0\1\12\1\32\13\12\4\0\1\12"+
    "\1\0\1\12\4\0\12\12\1\33\2\12\4\0\1\12"+
    "\1\0\1\12\4\0\7\12\1\34\5\12\4\0\1\12"+
    "\1\0\1\12\4\0\12\12\1\35\2\12\3\0\2\23"+
    "\1\0\1\23\2\0\1\23\1\0\16\23\1\0\1\23"+
    "\2\25\1\0\4\25\1\0\22\25\1\0\2\25\1\27"+
    "\1\25\1\0\20\25\7\0\1\31\21\0\1\12\1\0"+
    "\1\12\4\0\2\12\1\36\12\12\4\0\1\12\1\0"+
    "\1\12\4\0\6\12\1\37\6\12\4\0\1\12\1\0"+
    "\1\12\4\0\10\12\1\40\4\12\4\0\1\12\1\0"+
    "\1\12\4\0\1\12\1\41\13\12\4\0\1\12\1\0"+
    "\1\12\4\0\3\12\1\42\11\12\4\0\1\12\1\0"+
    "\1\12\4\0\6\12\1\43\6\12\4\0\1\12\1\0"+
    "\1\12\4\0\2\12\1\44\12\12\4\0\1\12\1\0"+
    "\1\12\4\0\5\12\1\45\7\12\4\0\1\12\1\0"+
    "\1\12\4\0\4\12\1\46\10\12\4\0\1\12\1\0"+
    "\1\12\4\0\5\12\1\47\7\12\4\0\1\12\1\0"+
    "\1\12\4\0\11\12\1\50\3\12\4\0\1\12\1\0"+
    "\1\12\4\0\5\12\1\51\7\12\4\0\1\12\1\0"+
    "\1\12\4\0\4\12\1\52\10\12\4\0\1\12\1\0"+
    "\1\12\4\0\5\12\1\53\7\12\4\0\1\12\1\0"+
    "\1\12\4\0\6\12\1\54\6\12\4\0\1\12\1\0"+
    "\1\12\4\0\10\12\1\55\4\12\4\0\1\12\1\0"+
    "\1\12\4\0\3\12\1\56\11\12\4\0\1\12\1\0"+
    "\1\12\4\0\5\12\1\57\7\12\4\0\1\12\1\0"+
    "\1\12\4\0\14\12\1\60\4\0\1\12\1\0\1\12"+
    "\4\0\12\12\1\61\2\12\4\0\1\12\1\0\1\12"+
    "\4\0\12\12\1\62\2\12\4\0\1\12\1\0\1\12"+
    "\4\0\13\12\1\63\1\12\4\0\1\12\1\0\1\12"+
    "\4\0\6\12\1\64\6\12\4\0\1\12\1\0\1\12"+
    "\4\0\11\12\1\65\3\12\4\0\1\12\1\0\1\12"+
    "\4\0\4\12\1\66\10\12\4\0\1\12\1\0\1\12"+
    "\4\0\5\12\1\67\7\12\3\0";

  private static int [] zzUnpackTrans() {
    int [] result = new int[1056];
    int offset = 0;
    offset = zzUnpackTrans(ZZ_TRANS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackTrans(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      value--;
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /* error codes */
  private static final int ZZ_UNKNOWN_ERROR = 0;
  private static final int ZZ_NO_MATCH = 1;
  private static final int ZZ_PUSHBACK_2BIG = 2;

  /* error messages for the codes above */
  private static final String[] ZZ_ERROR_MSG = {
    "Unknown internal scanner error",
    "Error: could not match input",
    "Error: pushback value was too large"
  };

  /**
   * ZZ_ATTRIBUTE[aState] contains the attributes of state <code>aState</code>
   */
  private static final int [] ZZ_ATTRIBUTE = zzUnpackAttribute();

  private static final String ZZ_ATTRIBUTE_PACKED_0 =
    "\5\0\1\1\1\11\10\1\2\11\2\1\1\11\1\1"+
    "\1\11\2\1\1\11\36\1";

  private static int [] zzUnpackAttribute() {
    int [] result = new int[55];
    int offset = 0;
    offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAttribute(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /** the input device */
  private java.io.Reader zzReader;

  /** the current state of the DFA */
  private int zzState;

  /** the current lexical state */
  private int zzLexicalState = YYINITIAL;

  /** this buffer contains the current text to be matched and is
      the source of the yytext() string */
  private CharSequence zzBuffer = "";

  /** the textposition at the last accepting state */
  private int zzMarkedPos;

  /** the current text position in the buffer */
  private int zzCurrentPos;

  /** startRead marks the beginning of the yytext() string in the buffer */
  private int zzStartRead;

  /** endRead marks the last character in the buffer, that has been read
      from input */
  private int zzEndRead;

  /**
   * zzAtBOL == true <=> the scanner is currently at the beginning of a line
   */
  private boolean zzAtBOL = true;

  /** zzAtEOF == true <=> the scanner is at the EOF */
  private boolean zzAtEOF;

  /** denotes if the user-EOF-code has already been executed */
  private boolean zzEOFDone;

  /* user code: */
    boolean dataJson = false;
    Character endingChar = '{';
    HashMap<Character, Character> endings = new HashMap<>();

    private List<Character> valueEndingChars(char current) {
        switch (current) {
            case '"':
                return List.of('"');
            case '(':
                return List.of(')');
            case '[':
                return List.of(']');
            default:
                return List.of(' ', ']');
        }
    }

    private boolean valueEndingIncluded(char current) {
        switch (current) {
            case '"':
                return true;
            case '(':
                return true;
            case '[':
                return true;
            default:
                return false;
        }
    }


  /**
   * Creates a new scanner
   *
   * @param   in  the java.io.Reader to read input from.
   */
  TscnLexer(java.io.Reader in) {
      endings.put('{', '}');
    endings.put('"', '"');
    this.zzReader = in;
  }


  /** 
   * Unpacks the compressed character translation table.
   *
   * @param packed   the packed character translation table
   * @return         the unpacked character translation table
   */
  private static char [] zzUnpackCMap(String packed) {
    int size = 0;
    for (int i = 0, length = packed.length(); i < length; i += 2) {
      size += packed.charAt(i);
    }
    char[] map = new char[size];
    int i = 0;  /* index in packed string  */
    int j = 0;  /* index in unpacked array */
    while (i < packed.length()) {
      int  count = packed.charAt(i++);
      char value = packed.charAt(i++);
      do map[j++] = value; while (--count > 0);
    }
    return map;
  }

  public final int getTokenStart() {
    return zzStartRead;
  }

  public final int getTokenEnd() {
    return getTokenStart() + yylength();
  }

  public void reset(CharSequence buffer, int start, int end, int initialState) {
    zzBuffer = buffer;
    zzCurrentPos = zzMarkedPos = zzStartRead = start;
    zzAtEOF  = false;
    zzAtBOL = true;
    zzEndRead = end;
    yybegin(initialState);
  }

  /**
   * Refills the input buffer.
   *
   * @return      {@code false}, iff there was new input.
   *
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  private boolean zzRefill() throws java.io.IOException {
    return true;
  }


  /**
   * Returns the current lexical state.
   */
  public final int yystate() {
    return zzLexicalState;
  }


  /**
   * Enters a new lexical state
   *
   * @param newState the new lexical state
   */
  public final void yybegin(int newState) {
    zzLexicalState = newState;
  }


  /**
   * Returns the text matched by the current regular expression.
   */
  public final CharSequence yytext() {
    return zzBuffer.subSequence(zzStartRead, zzMarkedPos);
  }


  /**
   * Returns the character at position {@code pos} from the
   * matched text.
   *
   * It is equivalent to yytext().charAt(pos), but faster
   *
   * @param pos the position of the character to fetch.
   *            A value from 0 to yylength()-1.
   *
   * @return the character at position pos
   */
  public final char yycharat(int pos) {
    return zzBuffer.charAt(zzStartRead+pos);
  }


  /**
   * Returns the length of the matched text region.
   */
  public final int yylength() {
    return zzMarkedPos-zzStartRead;
  }


  /**
   * Reports an error that occurred while scanning.
   *
   * In a wellformed scanner (no or only correct usage of
   * yypushback(int) and a match-all fallback rule) this method
   * will only be called with things that "Can't Possibly Happen".
   * If this method is called, something is seriously wrong
   * (e.g. a JFlex bug producing a faulty scanner etc.).
   *
   * Usual syntax/scanner level error handling should be done
   * in error fallback rules.
   *
   * @param   errorCode  the code of the errormessage to display
   */
  private void zzScanError(int errorCode) {
    String message;
    try {
      message = ZZ_ERROR_MSG[errorCode];
    }
    catch (ArrayIndexOutOfBoundsException e) {
      message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
    }

    throw new Error(message);
  }


  /**
   * Pushes the specified amount of characters back into the input stream.
   *
   * They will be read again by then next call of the scanning method
   *
   * @param number  the number of characters to be read again.
   *                This number must not be greater than yylength()!
   */
  public void yypushback(int number)  {
    if ( number > yylength() )
      zzScanError(ZZ_PUSHBACK_2BIG);

    zzMarkedPos -= number;
  }


  /**
   * Contains user EOF-code, which will be executed exactly once,
   * when the end of file is reached
   */
  private void zzDoEOF() {
    if (!zzEOFDone) {
      zzEOFDone = true;
    
    }
  }


  /**
   * Resumes scanning until the next regular expression is matched,
   * the end of input is encountered or an I/O-Error occurs.
   *
   * @return      the next token
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  public IElementType advance() throws java.io.IOException {
    int zzInput;
    int zzAction;

    // cached fields:
    int zzCurrentPosL;
    int zzMarkedPosL;
    int zzEndReadL = zzEndRead;
    CharSequence zzBufferL = zzBuffer;

    int [] zzTransL = ZZ_TRANS;
    int [] zzRowMapL = ZZ_ROWMAP;
    int [] zzAttrL = ZZ_ATTRIBUTE;

    while (true) {
      zzMarkedPosL = zzMarkedPos;

      zzAction = -1;

      zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;

      zzState = ZZ_LEXSTATE[zzLexicalState];

      // set up zzAction for empty match case:
      int zzAttributes = zzAttrL[zzState];
      if ( (zzAttributes & 1) == 1 ) {
        zzAction = zzState;
      }


      zzForAction: {
        while (true) {

          if (zzCurrentPosL < zzEndReadL) {
            zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL/*, zzEndReadL*/);
            zzCurrentPosL += Character.charCount(zzInput);
          }
          else if (zzAtEOF) {
            zzInput = YYEOF;
            break zzForAction;
          }
          else {
            // store back cached positions
            zzCurrentPos  = zzCurrentPosL;
            zzMarkedPos   = zzMarkedPosL;
            boolean eof = zzRefill();
            // get translated positions and possibly new buffer
            zzCurrentPosL  = zzCurrentPos;
            zzMarkedPosL   = zzMarkedPos;
            zzBufferL      = zzBuffer;
            zzEndReadL     = zzEndRead;
            if (eof) {
              zzInput = YYEOF;
              break zzForAction;
            }
            else {
              zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL/*, zzEndReadL*/);
              zzCurrentPosL += Character.charCount(zzInput);
            }
          }
          int zzNext = zzTransL[ zzRowMapL[zzState] + ZZ_CMAP(zzInput) ];
          if (zzNext == -1) break zzForAction;
          zzState = zzNext;

          zzAttributes = zzAttrL[zzState];
          if ( (zzAttributes & 1) == 1 ) {
            zzAction = zzState;
            zzMarkedPosL = zzCurrentPosL;
            if ( (zzAttributes & 8) == 8 ) break zzForAction;
          }

        }
      }

      // store back cached position
      zzMarkedPos = zzMarkedPosL;

      if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
        zzAtEOF = true;
        zzDoEOF();
              {
                if (yystate() == DATA_VALUE) {
        yybegin(YYINITIAL);
        return TscnTypes.VALUE;
    }
    return null;
              }
      }
      else {
        switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
          case 1: 
            { if (yytext().charAt(0) == '[') {
                          yypushback(yylength() - 1);
                          yybegin(HEADER);

                          return TscnTypes.LSBR;
                      } else {
                          yypushback(yylength() - 1);
                          yybegin(DATA_LINE);

                          continue;
                      }
            } 
            // fall through
          case 16: break;
          case 2: 
            { return TokenType.WHITE_SPACE;
            } 
            // fall through
          case 17: break;
          case 3: 
            { yybegin(HEADER); return TscnTypes.LSBR;
            } 
            // fall through
          case 18: break;
          case 4: 
            { return TscnTypes.IDENTIFIER;
            } 
            // fall through
          case 19: break;
          case 5: 
            { yybegin(YYINITIAL); return TscnTypes.RSBR;
            } 
            // fall through
          case 20: break;
          case 6: 
            { yybegin(VALUE); return TscnTypes.EQ;
            } 
            // fall through
          case 21: break;
          case 7: 
            { CharSequence line = yytext();
        Stack<Character> openings = new Stack<>();
        char currentOpening = line.charAt(0);
        List<Character> searchFor = valueEndingChars(currentOpening);
        List<Character> allowedOpening = List.of('"', '(', '[');
        boolean includeEnding = valueEndingIncluded(currentOpening);

        int i = 1;
        for (; i < line.length(); i++) {
            char current = line.charAt(i);
            if (searchFor.contains(current)) {
                if (openings.empty()) {
                    if (includeEnding) {
                       i++;
                    }
                    break;
                } else {
                    currentOpening = openings.pop();
                    includeEnding = valueEndingIncluded(currentOpening);
                    searchFor = valueEndingChars(currentOpening);
                }
            } else if (allowedOpening.contains(current)) {
                openings.push(currentOpening);
                currentOpening = current;
                includeEnding = valueEndingIncluded(currentOpening);
                searchFor = valueEndingChars(currentOpening);
            }
        }

        yypushback(line.length() - i);
        yybegin(HEADER);

        return TscnTypes.VALUE;
            } 
            // fall through
          case 22: break;
          case 8: 
            { yybegin(DATA_VALUE); return TscnTypes.EQ;
            } 
            // fall through
          case 23: break;
          case 9: 
            { String line = yytext().toString();
            if (line.startsWith("[") || line.contains(" = ")) {
                yypushback(yylength());
                yybegin(YYINITIAL);
                return TscnTypes.VALUE;
            } else {
                continue;
            }

//          String text = yytext().toString().trim();
//          char firstChar = text.charAt(0);
//          char lastChar = text.charAt(text.length() - 1);
//          if (dataJson) {
//              if (firstChar == endingChar || lastChar == endingChar) {
//                  if (endingChar == '}' && lastChar != '}') {
//                      continue; // TODO hack protože json může mít pole jsonů
//                  }
//
//                  dataJson = false;
//                  yybegin(YYINITIAL);
//                  return TscnTypes.VALUE;
//              }
//              continue;
//          } else {
//              if (firstChar == '{' || firstChar == '"') {
//                  endingChar = endings.get(firstChar);
//                  // Check oneliners
//                  if (lastChar == endingChar) {
//                      yybegin(YYINITIAL);
//                      return TscnTypes.VALUE;
//                  }
//
//                  dataJson = true;
//                  continue;
//              } else {
//                  yybegin(YYINITIAL);
//                  return TscnTypes.VALUE;
//              }
//          }
            } 
            // fall through
          case 24: break;
          case 10: 
            { //          if (dataJson) {
              continue;
//          } else {
//              return TokenType.WHITE_SPACE;
//          }
            } 
            // fall through
          case 25: break;
          case 11: 
            { return TscnTypes.COMMENT;
            } 
            // fall through
          case 26: break;
          case 12: 
            { return TscnTypes.NODE;
            } 
            // fall through
          case 27: break;
          case 13: 
            { return TscnTypes.GD_SCENE;
            } 
            // fall through
          case 28: break;
          case 14: 
            { return TscnTypes.CONNECTION;
            } 
            // fall through
          case 29: break;
          case 15: 
            { return TscnTypes.EXT_RESOURCE;
            } 
            // fall through
          case 30: break;
          default:
            zzScanError(ZZ_NO_MATCH);
          }
      }
    }
  }


}
